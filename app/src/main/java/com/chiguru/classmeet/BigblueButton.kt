package com.chiguru.classmeet



import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat.requestPermissions
import org.mozilla.geckoview.GeckoRuntime
import org.mozilla.geckoview.GeckoSession
import org.mozilla.geckoview.GeckoSession.PermissionDelegate
import org.mozilla.geckoview.GeckoSessionSettings
import org.mozilla.geckoview.GeckoSessionSettings.USER_AGENT_MODE_MOBILE
import org.mozilla.geckoview.GeckoView


private class BigBlueButtonPermissionDelegate : PermissionDelegate {
    private var mCallback: PermissionDelegate.Callback? = null
    fun onRequestPermissionsResult(
        permissions: Array<String?>?,
        grantResults: IntArray,
    ) {
        if (mCallback == null) {
            return
        }
        val cb: PermissionDelegate.Callback = mCallback as PermissionDelegate.Callback
        mCallback = null
        for (result in grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                // At least one permission was not granted.
                cb.reject()
                return
            }
        }
        cb.grant()
    }

//    override fun onAndroidPermissionsRequest(
//        session: GeckoSession,
//        permissions: Array<String>?,
//        callback: PermissionDelegate.Callback,
//    ) {
//        mCallback = callback
//        requestPermissions(permissions, androidPermissionRequestCode)
//    }
}


class BigblueButton : AppCompatActivity() {

    private lateinit var geckoView: GeckoView
    private val geckoSession = GeckoSession()


    private val TAG = "BigblueButton"

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
//        if (requestCode == REQUEST_PERMISSIONS ||
//            requestCode == REQUEST_WRITE_EXTERNAL_STORAGE
//        ) {
//            val permission: BigBlueButtonPermissionDelegate =
//                geckoSession.permissionDelegate as BigBlueButtonPermissionDelegate
//            permission.onRequestPermissionsResult(permissions, grantResults)
//        } else {
//            super.onRequestPermissionsResult(requestCode, permissions!!, grantResults!!)
//        }
    }

    private fun setupGeckoView() {
        geckoView = findViewById(R.id.gecko_view)
        val runtime = GeckoRuntime.create(this)


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val hasCameraPermission = checkSelfPermission(Manifest.permission.CAMERA)
            Log.d(TAG, "has camera permission: $hasCameraPermission")
            val hasRecordPermission = checkSelfPermission(Manifest.permission.RECORD_AUDIO)
            Log.d(TAG, "has record permission: $hasRecordPermission")
            val hasAudioPermission =
                checkSelfPermission(Manifest.permission.MODIFY_AUDIO_SETTINGS)
            Log.d(TAG, "has audio permission: $hasAudioPermission")
            val permissions: MutableList<String> = ArrayList()
            if (hasCameraPermission != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.CAMERA)
            }
            if (hasRecordPermission != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.RECORD_AUDIO)
            }
            if (hasAudioPermission != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.MODIFY_AUDIO_SETTINGS)
            }
            if (!permissions.isEmpty()) {
                requestPermissions(permissions.toTypedArray(), 111)
            }
        }

        geckoSession.open(runtime)

        val permission = BigBlueButtonPermissionDelegate()

        geckoSession.permissionDelegate = permission
        geckoSession.settings.apply {
            GeckoSessionSettings.Builder()
                .userAgentMode(USER_AGENT_MODE_MOBILE)
                .userAgentOverride("")
                .allowJavascript(true)
        }
        geckoView.setSession(geckoSession)


        val sessionId = intent.getStringExtra("MEETING_ID")
        val sessionPass = intent.getStringExtra("MEETING_PASSWORD")
        Toast.makeText(this, (sessionId), Toast.LENGTH_SHORT).show()
        geckoSession.loadUri("https://classmeet.chiguru.tech/app/${sessionId}")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bigblue_button)

        setupGeckoView()

    }

}



