package com.chiguru.classmeet.ui.join

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.chiguru.classmeet.BigblueButton
import com.chiguru.classmeet.MEETING_ID
import com.chiguru.classmeet.MEETING_PASSWORD
import com.chiguru.classmeet.R
import kotlinx.android.synthetic.main.fragment_join.*

class JoinFragment : Fragment(),View.OnClickListener {

    private lateinit var joinViewModel: JoinViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        joinViewModel =
                ViewModelProviders.of(this).get(JoinViewModel::class.java)
        val root:View = inflater.inflate(R.layout.fragment_join, container, false)

//        val sessionId:EditText = root.findViewById(R.id.meetingId)
//        val sessionPass:EditText = root.findViewById(R.id.meetingPassword)
        val joinBtn:Button = root.findViewById(R.id.join_btn)
        joinBtn.setOnClickListener(this)
        return root
    }


    override fun onClick(v: View?) {

        when(v?.id){
            R.id.join_btn -> {
                val sessionId:String = meetingId.text.toString()
                val sessionPass:String = meetingPassword.text.toString()

                val intent:Intent = Intent(activity, BigblueButton::class.java).apply {
                    putExtra(MEETING_ID,sessionId);
                    putExtra(MEETING_PASSWORD,sessionPass);
                }
                startActivity(intent)
            }

            else ->{
                Toast.makeText(activity, "not clicked", Toast.LENGTH_SHORT).show()
            }
        }
    }
}



