package com.chiguru.classmeet.ui.recording

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.chiguru.classmeet.R

class RecordingFragment : Fragment() {

    private lateinit var recordingViewModel: RecordingViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        recordingViewModel =
                ViewModelProviders.of(this).get(RecordingViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_recording, container, false)

        return root
    }
}